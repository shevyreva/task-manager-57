package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

@Service
public interface ITokenService {

    public String getToken();

    public void setToken(@Nullable final String token);

}
