package ru.t1.shevyreva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.user.UserRemoveRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class UserRemoveByLogin extends AbstractUserListener {

    @NotNull
    private final String DESCRIPTION = "User remove by login.";

    @NotNull
    private final String NAME = "user-remove-by-login";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRemoveByLogin.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE USER BY LOGIN]:");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login, null, null);
        userEndpoint.removeUser(request);

    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
