package ru.t1.shevyreva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.system.ServerAboutRequest;
import ru.t1.shevyreva.tm.dto.response.system.ServerAboutResponse;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    private final String DESCRIPTION = "Show about program.";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name" +
            "|| @applicationAboutListener.getArgument() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull ServerAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

}
