package ru.t1.shevyreva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.user.UserRemoveRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class UserRemoveByEmail extends AbstractUserListener {

    @NotNull
    private final String DESCRIPTION = "User remove by email.";

    @NotNull
    private final String NAME = "user-remove-by-email";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRemoveByEmail.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE USER BY EMAIL]:");
        System.out.println("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), null, email, null);
        userEndpoint.removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
