package ru.t1.shevyreva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.shevyreva.tm.listener.AbstractListener;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[TASK ID]: " + task.getId());
        System.out.println("[TASK NAME]: " + task.getName());
        System.out.println("[TASK DESCRIPTION]: " + task.getDescription());
        System.out.println("[TASK STATUS]: " + Status.toName(task.getStatus()));
        System.out.println("[PROJECT ID]: " + task.getProjectId());
    }

}
