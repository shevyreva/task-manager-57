package ru.t1.shevyreva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Remove task by Id.";

    @NotNull
    private final String NAME = "task-remove-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken(), id);
        taskEndpoint.removeTaskById(request);
    }

}
