package ru.t1.shevyreva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private final String DESCRIPTION = "Show about program.";

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name" +
            "|| @applicationHelpListener.getArgument() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (final AbstractListener listener : listeners)
            System.out.println(listener);
    }

}
