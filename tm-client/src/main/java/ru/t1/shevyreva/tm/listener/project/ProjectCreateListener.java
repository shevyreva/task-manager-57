package ru.t1.shevyreva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Create new project.";

    @NotNull
    private final String NAME = "project-create";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken(), name, description);
        projectEndpoint.createProject(request);
    }

}
