package ru.t1.shevyreva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    private final String DESCRIPTION = "Show argument list.";

    @NotNull
    private final String NAME = "arguments";

    @NotNull
    private final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name" +
            "@argumentListListener.getArgument() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener == null) continue;
            @Nullable final String argument = listener.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
