package ru.t1.shevyreva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IAbstractUserOwnedModelRepository<M extends AbstractUserOwnedModel> extends IAbstractModelRepository<M> {

    void addForUser(@NotNull String userId, M model);

    void updateForUser(@NotNull String userId, M model);

    void clearForUser(@NotNull String userId);

    void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    List<M> findAllForUser(@NotNull String userId);

    M findOneByIdForUser(@NotNull String userId, @NotNull String id);

    int getSizeForUser(@NotNull String userId);

}
