package ru.t1.shevyreva.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class ProjectDtoRepository extends AbstractUserOwnedModelDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public void clearForUser(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.userId  = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO")
                .executeUpdate();
    }

    @Override
    public List<ProjectDTO> findAllForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId  = :userId", ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class).getResultList();
    }

    @Override
    public List<ProjectDTO> findAll(@Nullable final Comparator<ProjectDTO> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM ProjectDTO e ORDER BY e." + sortType, ProjectDTO.class).getResultList();
    }

    @Override
    public List<ProjectDTO> findAllForUser(@NotNull final String userId, @Nullable final Comparator<ProjectDTO> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId  = :userId ORDER BY e." + sortType, ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public ProjectDTO findOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.id  = :id AND e.userId  = :userId", ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public ProjectDTO findOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId  = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public ProjectDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }


    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return findOneByIdForUser(userId, id) != null;
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
