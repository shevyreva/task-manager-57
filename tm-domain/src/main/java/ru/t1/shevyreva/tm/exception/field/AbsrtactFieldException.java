package ru.t1.shevyreva.tm.exception.field;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.exception.AbstractException;

public abstract class AbsrtactFieldException extends AbstractException {

    public AbsrtactFieldException() {
    }

    public AbsrtactFieldException(@Nullable final String message) {
        super(message);
    }

    public AbsrtactFieldException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbsrtactFieldException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbsrtactFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
