package ru.t1.shevyreva.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.comporator.DateComparator;
import ru.t1.shevyreva.tm.comporator.NameComparator;
import ru.t1.shevyreva.tm.comporator.StatusComparator;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;

import java.util.Comparator;

public enum ProjectDtoSort {

    BY_NAME("Sort by name.", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status.", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created.", DateComparator.INSTANCE::compare);

    @Nullable
    private final String displayName;

    @Nullable
    private final Comparator<ProjectDTO> comparator;

    ProjectDtoSort(@Nullable final String displayName, @Nullable final Comparator<ProjectDTO> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static ProjectDtoSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final ProjectDtoSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator<ProjectDTO> getComparator() {
        return comparator;
    }

}
